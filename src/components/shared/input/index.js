import React, {useRef, useEffect} from 'react';
import {useField} from '@unform/core';

import {Container, Box, Label, Bull, Error, TextInputs} from './styles';

const FormInput = ({name, label, ...rest}) => {
  const inputRef = useRef(null);

  const {fieldName, defaultValue, registerField, error} = useField(name);

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputRef.current,
      path: 'value',
      clearValue(ref) {
        ref.value = '';
        ref.clear();
      },
      setValue(ref, value) {
        ref.setNativeProps({text: value});
        inputRef.current.value = value;
      },
      getValue(ref) {
        return ref.value;
      },
    });
  }, [fieldName, registerField]);

  useEffect(() => {
    inputRef.current.value = defaultValue;
  }, [defaultValue]);

  return (
    <Container>
      <Bull>&bull;</Bull>
      <Box>
        <Label>{label}</Label>
        <TextInputs
          name={name}
          ref={inputRef}
          defaultValue={defaultValue}
          className={error ? 'has-error' : ''}
          onChangeText={(value) => {
            if (inputRef.current) {
              inputRef.current.value = value;
            }
          }}
          {...rest}
        />
      </Box>
      {error && <Error className="error">{error}</Error>}
    </Container>
  );
};

export default FormInput;
