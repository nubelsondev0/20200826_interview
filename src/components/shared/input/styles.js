import styled from 'styled-components';
import {Colors, Typography, Spacing} from '../../../resources/style';

export const Container = styled.View`
  width: ${(props) => (props.width ? props.width : '100%')};
  min-height: ${Spacing.SCALE_68};

  flex-direction: column;
`;

export const Box = styled.View`
  width: 100%;
  min-height: ${Spacing.SCALE_48};

  flex-direction: column;
  justify-content: center;
  align-items: flex-start;

  padding: 0 ${Spacing.SCALE_20};
  background: ${Colors.WHITE} 0% 0% no-repeat padding-box;
  border: ${Spacing.SCALE_1} solid ${Colors.GRAY_MEDIUM};
  border-radius: ${Spacing.SCALE_5};
  opacity: 1;
`;

export const TextInputs = styled.TextInput`
  width: 100%;

  font-size: ${Typography.FONT_SIZE_16};
  font-family: ${Typography.FONT_FAMILY_REGULAR};
  color: ${Colors.SECONDARY};
  padding: 0;
`;

export const Label = styled.Text`
  color: ${Colors.PRIMARY};
  font-size: ${Typography.FONT_SIZE_12};
  font-family: ${Typography.FONT_FAMILY_REGULAR};
`;

export const Bull = styled.Text`
  color: ${Colors.ALERT};
  position: absolute;
  right: -10px;
  top: -5px;
  z-index: 99;
`;

export const Error = styled.Text`
  color: ${Colors.ALERT};
  font-size: ${Typography.FONT_SIZE_12};
  font-family: ${Typography.FONT_FAMILY_REGULAR};
`;
