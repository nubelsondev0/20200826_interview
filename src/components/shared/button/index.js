import React from 'react';

import {Container, Label} from './styles';

const Button = ({formRef, children, ...rest}) => {
  return (
    <Container onPress={() => formRef.current.submitForm()} {...rest}>
      <Label>{children}</Label>
    </Container>
  );
};

export default Button;
