import styled from 'styled-components';
import {Colors, Spacing, Typography} from '../../../resources/style';

export const Container = styled.TouchableOpacity`
  width: ${(props) => (props.width ? props.width : '100%')};
  min-height: ${Spacing.SCALE_48};

  background: ${Colors.PRIMARY};
  border-radius: 5px;

  justify-content: center;
  align-items: center;

  elevation: 1;
`;

export const Label = styled.Text`
  color: ${Colors.WHITE};
  font-weight: bold;
  font-size: ${Typography.FONT_SIZE_16};
  font-family: ${Typography.FONT_FAMILY_REGULAR};
`;
