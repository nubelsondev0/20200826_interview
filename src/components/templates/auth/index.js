import React from 'react';

import {Container, Title, SubTitle} from './styles';

const Auth = ({children, title, subtitle}) => {
  return (
    <Container>
      {title && <Title>{title}</Title>}
      {subtitle && <SubTitle>{subtitle}:</SubTitle>}
      {children}
    </Container>
  );
};

export default Auth;
