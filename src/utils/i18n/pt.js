const pt = {
  email: 'E-mail',
  password: 'Senha',
  signIn: 'Entrar',
  recover: 'Recuperar',
  recoverPassword: 'Recuperar Senha',
  forgotPass: 'Esqueceu a senha',
  backToSignIn: 'Voltar ao login',
  enterYourDetails: 'Entre com seus dados abaixo',
  requirements: 'Requisitos',
  error: {
    isRequired: 'é obrigatório',
    isInvalid: 'é inválido',
    loginOrPassword: 'Login ou senha incorretos',
  },
};

export default pt;
