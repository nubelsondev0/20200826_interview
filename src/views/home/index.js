import React from 'react';
import {useDispatch} from 'react-redux';
import {SafeAreaView, Text} from 'react-native';

import {signOut} from '../../app/store/modules/auth/actions';

const Home = () => {
  const dispatch = useDispatch();
  return (
    <SafeAreaView>
      <Text>Olá</Text>
    </SafeAreaView>
  );
};

export default Home;
