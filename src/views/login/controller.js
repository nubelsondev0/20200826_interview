import {signInRequest} from '../../app/store/modules/auth/actions';
import * as Yup from 'yup';

export const doSchema = ({Dictionary}) => {
  return Yup.object().shape({
    email: Yup.string()
      .email(`${Dictionary.email} ${Dictionary.error.isInvalid}`)
      .required(`${Dictionary.email} ${Dictionary.error.isRequired}`),
    password: Yup.string().required(
      `${Dictionary.password} ${Dictionary.error.isRequired}`,
    ),
  });
};

export const doSubmit = async ({formRef, dispatch}) => {
  const data = formRef.current.getData();

  const {email, password} = data;

  dispatch(signInRequest('', password));
};

export const handleIncorrectPwd = ({signInFail, formRef, Dictionary}) => {
  const data = formRef.current.getData();

  if (signInFail && data.email !== undefined && data.password !== undefined) {
    formRef.current.setFieldError('password', Dictionary.error.loginOrPassword);
  }
};
