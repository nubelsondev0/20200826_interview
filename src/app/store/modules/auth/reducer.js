import produce from 'immer';
import {addSeconds} from 'date-fns';

const INITIAL_STATE = {
  user: null,
  token: {},
  loading: false,
  credentials: {},
  signInFail: false,
};

export default function auth(state = INITIAL_STATE, action = {}) {
  return produce(state, (draft) => {
    switch (action.type) {
      case '@auth/RECOVER_REQUEST': {
        draft.loading = true;
        break;
      }
      case '@user/SIGN_UP_REQUEST':
      case '@auth/SIGN_IN_REQUEST':
        //TODO: remove this once refresh_token is working
        draft.credentials = {...action.payload};
        draft.loading = true;
        draft.signInFail = false;
        break;
      case '@auth/RECOVER_SUCCESS':
        draft.loading = false;
        break;
      case '@auth/SIGN_FAILURE': {
        draft.loading = false;
        draft.signInFail = true;
        break;
      }
      case '@auth/SIGN_IN_SUCCESS': {
        draft.token = {
          ...action.payload.token,
          expire_date: addSeconds(new Date(), action.payload.token.expires_in),
        };
        draft.signed = true;
        draft.loading = false;
        draft.signInFail = false;
        break;
      }
      case '@auth/SIGN_OUT': {
        draft.applicationToken = null;
        draft.signed = false;
        draft.credentials = {};
        draft.loading = false;
        draft.signInFail = false;
        break;
      }
      default:
    }
  });
}
