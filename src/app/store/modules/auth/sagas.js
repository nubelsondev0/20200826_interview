import {takeLatest, all, put, call} from 'redux-saga/effects';

import {store} from '../../../store';
import {oAuthRequest, api} from '../../../api';
import {signInSuccess, signFailure, recoverSuccess} from './actions';

export function* singIn({payload}) {
  try {
    const {email, password} = payload;

    const response = yield call(oAuthRequest, 'oauth/token', {
      username: email,
      password,
      grant_type: 'password',
    });

    if (response.status === 200) {
      yield put(signInSuccess(response.data));
    }
  } catch (err) {
    yield put(signFailure());
  }
}

export function* recoverRequest({payload}) {
  const lanCode = store.getState().language.code;
  try {
    const {email} = payload;

    yield call(api.post, 'utilizador/senha/recuperar', {
      email,
      lingua: lanCode,
    });

    yield put(recoverSuccess());
  } catch (err) {
    // console.tron.log(err.message);
  }
}

export default all([
  takeLatest('@auth/SIGN_IN_REQUEST', singIn),
  takeLatest('@auth/RECOVER_REQUEST', recoverRequest),
]);
