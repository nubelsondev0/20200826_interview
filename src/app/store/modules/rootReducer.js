import {combineReducers} from 'redux';

import auth from './auth/reducer';
import language from './language/reducer';

export default combineReducers({
  auth,
  language,
});
